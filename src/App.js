
import './App.css';
import './scss/App.scss';


//import Pages
import Homepage from './Pages/Homepage';

function App() {
  return (
    <div className="App">
      <Homepage></Homepage>
    </div>
  );
}

export default App;
