import React from 'react'

//import image
import DotIcon from '../images/dot.svg';
import FbIcon from '../images/fb.svg';
import TwIcon from '../images/tw.svg';
import IgIcon from '../images/ig.svg';
import YtIcon from '../images/yt.svg';
import LinkIcon from '../images/link.svg';
import FollowIcon from '../images/follow.svg';
import FollowingIcon from '../images/following.svg';


//importComponents

export default class Card extends React.Component {
    constructor(props){ 
        super(props) 
            
        this.state = { 
            isFollow : false,
            isShowMenu : false,
        } 
            
        this.handleAddFriend = this.handleAddFriend.bind(this) ;
        this.handeShowMenu = this.handeShowMenu.bind(this);
    } 

    handleAddFriend(){ 
        this.setState(prevState => ({
            isFollow: !prevState.isFollow
        }));
    } 

    handeShowMenu() {
        this.setState(prevState => ({
            isShowMenu: !prevState.isShowMenu
        }));
    }

    handleAlert() {
        alert('Menuju link tujuan')
    }
    

    render(){
        const { isFollow, isShowMenu } = this.state;
        return(
            <div className="mainCard">
                <div className="mainCard__inner">
                    <div className="profileBanner" style ={ { backgroundImage: "url("+this.props.bannerImg+")" } }>
                        <button onClick={this.handeShowMenu} className="settingBtn"><img src={DotIcon}></img></button>
                        <div className={`tooltips ${!isShowMenu ? "" : "show"}`}>
                            <a href="#" onClick={this.handeShowMenu}>Report akun</a>
                        </div>
                    </div>
                    <div className="profilePhoto">
                        <div className="profileImg" style ={ { backgroundImage: "url("+this.props.profileImg+")" } }></div>
                        <button onClick={this.handleAddFriend} className={`FollowBtn ${!isFollow ? "" : "greyBtn"}`}><img src={`${!isFollow ? FollowIcon : FollowingIcon }`}></img>{`${!isFollow ? "Follow" : "Following"}`}</button>
                    </div>
                    <div className="profileBio">
                        <div className="profileName">
                         <h5>{this.props.name}</h5><p>@{this.props.id}</p>
                        </div>
                        <p className="bio">{this.props.bio}</p>
                    </div>
                    <div className="profileStat">
                        <h5 className="followers">{this.props.follower}k <span>Followers</span></h5>
                        <span className="circleSparator"></span>
                        <h6 className="prof">{this.props.prof}</h6>
                    </div>
                    <div className="socialMediaBox">
                        <a href="#" onClick={this.handleAlert} className="SocialMeidaBtn"><img src={FbIcon}></img></a>
                        <a href="#" onClick={this.handleAlert} className="SocialMeidaBtn"><img src={TwIcon}></img></a>
                        <a href="#" onClick={this.handleAlert} className="SocialMeidaBtn"><img src={IgIcon}></img></a>
                        <a href="#" onClick={this.handleAlert} className="SocialMeidaBtn"><img src={YtIcon}></img></a>
                        <a href="#" onClick={this.handleAlert} className="SocialMeidaBtn"><img src={LinkIcon}></img></a>
                    </div>
                </div>
            </div>
        )
    }
    
}