import React, { useEffect } from 'react';

//import components
import ProfileCard from '../Components/Card';


//import Banner

import ProfilePhoto from '../images/prof.jpeg';
import BannerBg from '../images/profile-banner.jpg';
import ProfilePhotoSample from '../images/prof2.jpeg';
import BannerBgSample from '../images/banner2.jpg';

export default function Homepages() {
    
    
    useEffect(() => {
        document.title = "FED Assignment - Fahmi Sabar"
     }, []);

    return(
      <div className="main-container">
      <div className="main-container__inner">
        <div className="cardGrid">
          <div className="cardGrid__items">
            <ProfileCard 
                name="Evelyn"
                id="FeraJelly"
                bio="Hola! Fera Jelita is here! Let's trakteer kerupuk udang! Thanks love!✨"
                follower="1.3"
                prof="Cosplay"
                bannerImg={BannerBg}
                profileImg={ProfilePhoto}
            />
            <ProfileCard 
                name="Jakob Sutisna Nasution ahmad wirya marbuana sentosa maya sari"
                id="Jakkkkkkoby"
                bio="Jakkobe suka makan bakso pake nasi, yoooo trakteer dong biar aku bisa beli bakso setan laknatullah, bair makin semangat makan baksonya yooo ahh gassssssssss.sekarang yak jangan pakai lama okeh bos!"
                follower="666"
                prof="Cosplay, KangGibah, Suka-Makan, Review Makanan, dan Lain Lain"
                bannerImg={BannerBgSample}
                profileImg={ProfilePhotoSample}
            />
            <ProfileCard 
                name="Evelyn"
                id="FeraJelly"
                bio="Hola! Fera Jelita is here! Let's trakteer kerupuk udang! Thanks love!✨"
                follower="1.3"
                prof="Cosplay"
                bannerImg={BannerBg}
                profileImg={ProfilePhoto}
            />
          </div>
        </div>
      </div>
    </div>
    )
}

